# Game of Life: An Implementation Of

An implementation of John Conway's Game of Life written in Python. There are
good overviews ([here](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life),
[here](https://web.stanford.edu/~cdebs/GameOfLife/#history)) but in short:
- it is a zero-player game,
- the universe consists of a two-dimensional matrix of cells,
- each cell is either 'dead' or 'alive',
- and a cell's status in the next iteration/frame/step is dependent on the
  current status of the cells surrounding it.

I will be following a set of rules:
- any live cell with fewer than two live neighbours dies,
- any live cell with more than three live neighbours dies,
- any live cell with two or three live neighbours continues living,
- and any dead cell with exactly three live neighbours becomes alive.

## Milestones
- ~~Print out matrix on terminal.~~
- ~~Hard-code first iteration.~~
- ~~Create lifes.~~
- ~~Test other oscillators + oscillations beyond range of matrix.~~
- ~~Figure out detection of cell outside of range.~~
- Implement tests: unittest
- Use of some GUI perhaps?
~~- Other ways to introduce first iteration (still on CLI).~~
- ~~Game class~~
~~- RLE (or input file) parsing~~
